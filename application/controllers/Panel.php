<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Panel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('panel_');
    }


    public function index()
    {
        $data['clients'] = $this->panel_->get_summary(1)->result();
        $data['projects'] = $this->panel_->get_summary(2)->result();
        $data['title'] = 'Panel Admin';
        $data['view'] = 'panel/index';
        $this->load->view('template/main', $data);
    }

    function form()
    {
        $data['title'] = 'Panel Admin';
        $data['view'] = 'panel/form';
        $this->load->view('template/main', $data);
    }

    function insert()
    {
        $id = $this->input->post('id');

        $data = [
            'jenis' => $this->input->post('jenis'),
            'status' => $this->input->post('status'),
            'deskripsi' => $this->input->post('deskripsi'),
            'perusahaan' => $this->input->post('perusahaan'),
        ];

        if ($id) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Success updated!</div>');
            $this->db->update('summary', $data, ['id' => $id]);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Success inserted!</div>');
            $this->db->insert('summary', $data);
        }
        redirect('panel');
    }

    function edit()
    {
        $id = $this->input->post('id');

        $data = $this->db->get_where('summary', ['id' => $id])->row();

        echo json_encode($data);
    }

    function delete($id)
    {
        if ($this->db->delete('summary', ['id' => $id])) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Success deleted!</div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Failed deleted!</div>');
        }
        redirect('panel');
    }
}
