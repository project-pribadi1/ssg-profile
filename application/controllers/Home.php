<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function index()
    {
        $data['title'] = 'Home';
        $data['view'] = 'home';
        $this->load->view('template/main', $data);
    }

    function about()
    {
        $data['title'] = 'About';
        $data['view'] = 'about';
        $this->load->view('template/main', $data);
    }

    function contact()
    {
        $data['title'] = 'Contact';
        $data['view'] = 'contact';
        $this->load->view('template/main', $data);
    }

    function service()
    {
        $data['title'] = 'Service';
        $data['view'] = 'service';
        $this->load->view('template/main', $data);
    }

    function client()
    {
        $this->load->model('panel_');
        $data['oil_gas'] = $this->panel_->get_summary(1, 1)->result();
        $data['marine'] = $this->panel_->get_summary(1, 2)->result();
        $data['civil'] = $this->panel_->get_summary(1, 3)->result();
        $data['title'] = 'Client';
        $data['view'] = 'clients';
        $this->load->view('template/main', $data);
    }

    function project()
    {
        $this->load->model('panel_');
        $data['oil_gas'] = $this->panel_->get_summary(2, 1)->result();
        $data['marine'] = $this->panel_->get_summary(2, 2)->result();
        $data['civil'] = $this->panel_->get_summary(2, 3)->result();
        $data['title'] = 'Project';
        $data['view'] = 'project';
        $this->load->view('template/main', $data);
    }

    function civil_construction($id = null)
    {
        $data['title'] = 'Civil Construction';
        $data['view'] = 'detail/civil-construction';
        $this->load->view('template/main', $data);
    }

    function kapel_villa($id = null)
    {
        $data['title'] = 'Kapel Villa';
        $data['view'] = 'detail/kapel-villa';
        $this->load->view('template/main', $data);
    }
    function shimano($id = null)
    {
        $data['title'] = 'PT Shimano';
        $data['view'] = 'detail/shimano';
        $this->load->view('template/main', $data);
    }

    function philips($id = null)
    {
        $data['title'] = 'PT Philips';
        $data['view'] = 'detail/philips';
        $this->load->view('template/main', $data);
    }

    function twa_wisata_alam($id = null)
    {
        $data['title'] = 'TWA Wisata Alam';
        $data['view'] = 'detail/wisata-alam';
        $this->load->view('template/main', $data);
    }

    function detail()
    {
        $data['title'] = 'Project Refrence';
        $data['view'] = 'detail';
        $this->load->view('template/main', $data);
    }

    function civil_contruction()
    {
        $data['title'] = 'Galery Civil Constructions';
        $data['view'] = 'galery/civil-constructions';
        $this->load->view('template/main', $data);
    }

    function oil_gas()
    {
        $data['title'] = 'Galery Oil and Gas';
        $data['view'] = 'galery/oil_gas';
        $this->load->view('template/main', $data);
    }

    function marine()
    {
        $data['title'] = 'Galery Marine';
        $data['view'] = 'galery/marine';
        $this->load->view('template/main', $data);
    }

    function not_found()
    {
        $data['title'] = 'Page Not Found';
        $data['view'] = 'not_found';
        $this->load->view('template/main', $data);
    }
}
