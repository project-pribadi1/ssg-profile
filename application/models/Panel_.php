<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Panel_ extends CI_Model
{
    function get_summary($jenis = null, $status = null)
    {
        $this->db->select('*');
        if ($jenis) {
            $this->db->where('jenis', $jenis);
        }
        if ($status) {
            $this->db->where('status', $status);
        }
        return $this->db->get('summary');
    }
}
