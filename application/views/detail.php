<section class="content-section section-2 padding">
    <div class="container">
        <div class="row project-single-wrap align-items-center">
            <div class="col-md-6 sm-padding">
                <div class="project-single-content">
                    <h2>Kapel Villa</h2>
                    <p>Indico is a different kind of architecture practice. Founded by LoganCee in 1991, we’re an
                        employee-owned firm pursuing a democratic design process that values everyone’s input.</p>
                    <ul class="project-details">
                        <li><span>Type</span>: Civil Construction</li>
                        <li><span>Architects</span>: José Carpio, Valentin Lacoste, Kyle Frederick</li>
                        <li><span>Location</span>: 962 Fifth Avenue, 3rd Floor New York.</li>
                        <li><span>Category</span>: Architecture, Interior.</li>
                        <li><span>Area</span>: 119.0.563</li>
                        <li><span>Project Year</span>: 2019/2020</li>
                        <li><span>Manufactures</span>: AlexaTheme construction company.</li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6 sm-padding">
                <div id="project-single-carousel" class="project-single-carousel box-shadow owl-carousel">
                    <?php for ($i = 1; $i < 12; $i++) { ?>
                        <div class="single-carousel">
                            <img src="<?= base_url('assets/') ?>img/project/civil-constructions/kapel-villa/villa-<?= $i ?>-min.jpg" alt="img">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>