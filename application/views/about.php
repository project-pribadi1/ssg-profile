<section class="page-header padding">
    <div class="container">
        <div class="page-content text-center">
            <h2>About PT Sumber Sukses Ganda</h2>
            <h3 class="text-white">Since 2005, We are <strong>Panbil Group of Company</strong></h3>
            <p>Marine - Offshore - Oil & Gas - Petrochemicals - Industries Contractor & Supplier.</p>
        </div>
    </div>
</section>
<section class="service-section section-2 bg-grey padding">
    <!-- <div class="dots"></div> -->
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-lg-8 col-md-6 sm-padding">
                <div class="section-heading mb-40">
                    <!-- <span>Projects</span> -->
                    <h2>Our Service and Commitment</h2>
                </div>
            </div>
        </div>
        <div class="row d-flex align-items-center">
            <div class="col-lg-6 sm-padding">
                <div class="service-content wow fadeInLeft">
                    <!-- <span>Commitment to Health & Safety</span> -->
                    <h3>Commitment to Health & Safety</h3>
                    <h5>Contracting is committed to health and safety of its workforce and the preservation of the environment for benefit of families today and future generations :</h5>

                    <li>
                        An established global safety, health and environment management system based upon regulatory requirements and recognized best practices.
                    </li>
                    <li>
                        We will continually promote employee safety on and off the job.
                    </li>
                    <li>
                        We believe all occupational injuries and illnesses are preventable.
                    </li>
                    <li>
                        Regularly assessing and auditing all our operations to measure conformance to our policies and standards with the goal of continuous improvement.
                    </li>
                    <li>
                        Working with customers, suppliers, local communities, government agencies, and industry groups
                        to meet our safety, health and environmental responsibilities.
                    </li>
                    <!-- <p>Building isn’t just job. It's our passion. With every project we undertake, we set the bar high and provide the best industry.</p> -->
                    <p></p>
                    <!-- <a href="#" class="default-btn">Our All Services</a> -->
                </div>
            </div>
            <div class="col-lg-6 sm-padding">
                <div class="row services-list">
                    <div class="col-md-6 padding-15">
                        <div class="service-item box-shadow wow fadeInUp" data-wow-delay="100ms">
                            <i class="flaticon-loader"></i>
                            <h4>Piping & Plumbing Work</h4>
                            <p>Plumbing System, Pipe Spool Facrication, Pipe Installation and Hydrotest </p>
                        </div>
                    </div>
                    <div class="col-md-6 padding-15 offset-top">
                        <div class="service-item box-shadow wow fadeInUp" data-wow-delay="300ms">
                            <i class="flaticon-tanks"></i>
                            <h4>Blasting and Painting Work</h4>
                            <p>Sand Blasting and Power Tooling, Painting and Touh up</p>
                        </div>
                    </div>
                    <div class="col-md-6 padding-15">
                        <div class="service-item box-shadow wow fadeInUp" data-wow-delay="400ms">
                            <i class="flaticon-refinery"></i>
                            <h4>Civil & Steel Construction Engineering & Design</h4>
                            <p>
                                Civil Building Construction, Steel Construction, Ship Building, Hull Fabrication and Erection, Out Fittings Fabrication and Installation, and Firing and Bending
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 padding-15 offset-top">
                        <div class="service-item box-shadow wow fadeInUp" data-wow-delay="500ms">
                            <i class="flaticon-control-system"></i>
                            <h4>Electrical & Instrument Work</h4>
                            <p>MSB Panel Builder, Electrical Steel Work Fabrication & Installation, Pulling Cable & Tie Up, Line Check and Measuring & Tag, and Termination & Connection, Partial Test, General Test, Sea Trial .</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>