<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/') ?>img/favicon.png">
    <link rel="icon" type="image/x-icon" href="<?= base_url('assets/') ?>img/favicon.png">
    <!-- untuk  penulis dari website-->
    <meta name="author" content="@mhmd_sahyudi">
    <!-- untuk memberikan description pada search enggine -->
    <meta name="keywords" content="Our Busines is Industrial Factory, Refinery, Power Plant, Marine, Offshore, Oil&Gas, Contractor">
    <!-- untuk keyword yang akan di cari search enggine -->
    <meta name="description" content="PT. SUMBER SUKSES GANDA has founded since 2005, we are Panbil Group of Company. Our Busines is Industrial Factory, Refinery, Power Plant, Marine, Offshore, Oil&Gas, Contractor">

    <meta property="og:title" content="PT Sumber Sukses Ganda | <?= ($title) ? $title :  'Home' ?>">
    <meta property="og:description" content="PT. SUMBER SUKSES GANDA has founded since 2005, we are Panbil Group of Company. Our Busines is Industrial Factory, Refinery, Power Plant, Marine, Offshore, Oil&Gas, Contractor">
    <meta property="og:image" content="<?= base_url('assets/') ?>img/favicon.png">
    <meta property="og:url" content="http://ssgbatam.co.id/">
    <meta property="og:type" content="website" />

    <meta name="twitter:card" content="summary_large_image">

    <!--  Non-Essential, But Recommended -->

    <meta property="og:site_name" content="@mhmd_sahyudi">
    <meta name="twitter:image:alt" content="Jastip Produk Chinas">


    <!--  Non-Essential, But Required for Analytics -->

    <meta property="fb:app_id" content="https://www.facebook.com/PT-Sumber-Sukses-Ganda-449071075871100/" />
    <meta name="twitter:site" content="@shopsolution.cn">

    <title> PT Sumber Sukses Ganda | <?= ($title) ? $title :  'Home' ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/') ?>img/favicon.png">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/fontawesome.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/themify-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/elegant-line-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/elegant-font-icons.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/flaticon.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/animate.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/slick.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/slider.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/odometer.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/venobox/venobox.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/main.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <script src="<?= base_url('assets/sweetalert2/sweetalert2@9.js') ?>"></script>

    <style>
        p {
            color: #8d9aa8;
        }
    </style>
    <!-- script -->
    <script src="<?= base_url('assets/') ?>js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <!-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> -->
    <script src="<?= base_url('assets/') ?>js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/bootstrap.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/tether.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/headroom.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/owl.carousel.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/smooth-scroll.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/venobox.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/jquery.ajaxchimp.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/slick.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/waypoints.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/odometer.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/vendor/wow.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</head>

<body>
    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>
    <header class="header">
        <div class="primary-header bg-white">
            <div class="container">
                <div class="primary-header-inner ">
                    <div class="header-logo m-1">
                        <a href="<?= base_url('home') ?>"><img src="<?= base_url('assets/') ?>img/logo-new.png" alt="Indico">
                        </a>
                    </div>
                    <div class="header-menu-wrap">
                        <ul class="dl-menu">
                            <li class="active">
                                <a href="<?= base_url('home') ?>" class="dl-btn">Home</a>
                            </li>
                            <li>
                                <a href="<?= base_url('home') ?>#about">About</a>
                            </li>
                            <li>
                                <a href="<?= base_url('service') ?>">Services</a>
                            </li>
                            <li>
                                <a href="<?= base_url('project') ?>">Projects</a>
                            </li>
                            <li>
                                <a href="<?= base_url('client') ?>">Clients</a>
                            </li>
                            <li><a href="<?= base_url('contact') ?>">Contact</a></li>
                        </ul>
                    </div>
                    <div class="header-right">
                        <div class="mobile-menu-icon">
                            <div class="burger-menu">
                                <div class="line-menu line-half first-line"></div>
                                <div class="line-menu"></div>
                                <div class="line-menu line-half last-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>