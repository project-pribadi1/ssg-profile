<section class="projects-section padding pt-5 data-aos=" fade-up" data-aos-anchor-placement="top-center"">
    <div class=" container">
    <div class="section-title">
        <h2>Galery</h2>
        <p>Our Projects Galery</p>
    </div>

    <div class="row" data-aos="fade-left">
        <div class="col-lg-4 col-md-6">
            <div class="member" data-aos="zoom-in" data-aos-delay="100">
                <div class="pic project-item">
                    <img src="<?= base_url('assets/img/project/oil-gas/oil-gas-2.JPG') ?>" class="img-fluid" alt="project">
                    <div class="overlay"></div>
                    <div class="projects-content">
                        <a href="<?= base_url('oil_gas') ?>" class="category">
                            <h3>Oil and Gas</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5 mt-md-0">
            <div class="member" data-aos="zoom-in" data-aos-delay="200">
                <div class="pic project-item">
                    <img src="<?= base_url('assets/img/project/marine/marine-1.jpeg') ?>" class="img-fluid" alt="project">
                    <div class="overlay"></div>
                    <div class="projects-content">
                        <a href="<?= base_url('marine') ?>" class="category">
                            <h3>Marine Offshore</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-5 mt-lg-0">
            <div class="member" data-aos="zoom-in" data-aos-delay="300">
                <div class="pic project-item">
                    <img src="<?= base_url('assets/img/project/civil-constructions/wisata-alam.jpg') ?>" class="img-fluid" alt="project">
                    <div class="overlay"></div>
                    <div class="projects-content">
                        <a href="<?= base_url('civil_constructions') ?>" class="category">
                            <h3>Civil Construction</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- End Team Section -->
<!-- footer -->
<section class="widget-section padding" data-aos="fade-up" data-aos-delay="400">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-6 sm-padding">
                <div class="widget-content">
                    <h4>PT SUMBER SUKSES GANDA</h4>
                    <a href="<?= base_url('home') ?>"><img src="<?= base_url('assets/') ?>img/logo-new.png" alt="brand"></a>
                    <p style="font-size: 20px;">Marine - Offshore - Oil & Gas - Petrochemicals - Industries Contractor & Supplier.</p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 sm-padding">
                <div class="widget-content">
                    <h4>Address</h4>
                    <p>Panbil Plaza Lantai 3,<br> Jl. Ahmad Yani - Muka kuning Batam 29433, <br> Indonesia</p>
                    <h4>Email:</h4>
                    <p> marudutmarius@hotmail.com</p>
                    <p>marudutsimalango@gmail.com</p>
                    <h4>Phone:</h4>
                    <p>0778 371642</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 sm-padding">
                <div class="widget-content">
                    <h4>Company</h4>
                    <ul class="widget-links">
                        <li><a href="<?= base_url('home') ?>#about">About</a></li>
                        <li><a href="<?= base_url('service') ?>">Services</a></li>
                        <li><a href="<?= base_url('client') ?>">Clients</a></li>
                        <li><a href="<?= base_url('project') ?>">Project</a></li>
                        <li><a href="<?= base_url('contact') ?>">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <hr> -->
        <div class="row mb-0">
            <div class="col-lg-8 col-md-10 mx-auto">
                <ul class="list-inline text-center">
                    <li class="list-inline-item">
                        <a href="https://www.youtube.com/channel/UCvkzLq4WrsIZIpyWadlFgRg">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-youtube fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://wa.me/6287883600089">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-whatsapp fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/PT-Sumber-Sukses-Ganda-449071075871100/">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>



<!-- <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_carrot-up"></i></a> -->
<script src="<?= base_url('assets/') ?>js/main.js"></script>
<script>
    AOS.init();
    $('.table').DataTable();
    (function() {
        var options = { // Call phone number
            whatsapp: "6287883600089",
            greeting_message: "Welcome to Sumber Sukses Ganda", // Text of greeting message
            call_to_action: "Chat With Us", // Call to action
            button_color: "#000000", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,line,call,email, whatsapp" // Order of buttons
        };
        var proto = "https:",
            host = "whatshelp.io",
            url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function() {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
</body>

</html>