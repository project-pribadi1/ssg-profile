<section class="service-section section-2 padding">
    <!-- <div class="dots"></div> -->
    <div class="container">

        <h3 class="mb-3">Marine Offshore</h3>
        <div id="carouselExampleIndicators-1" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php for ($i = 1; $i < 23; $i++) { ?>
                    <li data-target="#carouselExampleIndicators-1" data-slide-to="<?= $i ?>" class="<?= ($i == 1) ? 'active' : '' ?>"></li>
                <?php } ?>
            </ol>
            <div class="carousel-inner">
                <?php for ($i = 1; $i < 23; $i++) { ?>
                    <div class="carousel-item <?= ($i == 1) ? 'active' : '' ?>">
                        <img class="d-block w-100" src="<?= base_url('assets/') ?>img/project/marine/marine-<?= $i ?>.jpeg">
                    </div>
                <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators-1" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators-1" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>