<section class="service-section section-2 bg-grey padding">
    <div class="dots"></div>
    <div class="container">
        <div class="row project-single-wrap align-items-center">
            <div class="col-md-6 sm-padding">
                <div id="project-single-carousel" class="project-single-carousel box-shadow owl-carousel">
                    <?php for ($i = 1; $i < 7; $i++) { ?>
                        <div class="single-carousel">
                            <img src="<?= base_url('assets/') ?>img/project/oil-gas/oil-gas-<?= $i ?>.jpg" alt="img">
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-6 sm-padding">
                <div class="row services-list">
                    <div class="col-md-6 padding-15">
                        <div class="service-item box-shadow wow fadeInUp" data-wow-delay="100ms">
                            <h4>Oil and Gas</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>