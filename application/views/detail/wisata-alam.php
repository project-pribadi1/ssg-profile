<section class="service-section section-2 padding">
    <!-- <div class="dots"></div> -->
    <div class="container">
        <div class="row project-single-wrap align-items-center">
            <div class="col-md-6 sm-padding">
                <div class="project-single-content">
                    <h2>TWA Wisata Alam</h2>
                    <!-- <p>Indico is a different kind of architecture practice. Founded by LoganCee in 1991, we’re an
                        employee-owned firm pursuing a democratic design process that values everyone’s input.</p> -->
                    <ul class="project-details">
                        <li><span>Type</span>: Civil Construction</li>
                        <!-- <li><span>Architects</span>: José Carpio, Valentin Lacoste, Kyle Frederick</li>
                        <li><span>Location</span>: 962 Fifth Avenue, 3rd Floor New York.</li>
                        <li><span>Category</span>: Architecture, Interior.</li>
                        <li><span>Area</span>: 119.0.563</li>
                        <li><span>Project Year</span>: 2019/2020</li>
                        <li><span>Manufactures</span>: AlexaTheme construction company.</li> -->
                    </ul>
                </div>
            </div>

            <div class="col-md-6 sm-padding">
                <div id="project-single-carousel" class="project-single-carousel box-shadow owl-carousel">
                    <?php for ($i = 1; $i < 11; $i++) { ?>
                        <div class="single-carousel">
                            <img src="<?= base_url('assets/') ?>img/project/civil-constructions/twa-wisata-alam/alam-<?= $i ?>.jpg" alt="img">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="projects-section padding">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-lg-8 col-md-6 sm-padding">
                <div class="section-heading mb-40">
                    <!-- <span>Projects</span> -->
                    <h2>Projects Reference</h2>
                </div>
            </div>
            <!-- <div class="col-lg-4 col-md-6 sm-padding text-right">
                <a href="#" class="default-btn">View All Projects</a>
            </div> -->
        </div>
        <div id="projects-carousel" class="projects-carousel box-shadow owl-carousel">
            <div class="project-item">
                <img src="<?= base_url('assets/') ?>img/project/civil-constructions/shimano/civil-1.jpg" alt="projects">
                <div class="overlay"></div>
                <div class="projects-content">
                    <a href="#" class="category">Civil Construction</a>
                    <h3><a href="<?= base_url('shimano') ?>" class="tittle">PT Shimano</a></h3>
                </div>
            </div>
            <div class="project-item">
                <img src="<?= base_url('assets/') ?>img/project/civil-constructions/philips/philips-1.jpg" alt="projects">
                <div class="overlay"></div>
                <div class="projects-content">
                    <a href="#" class="category">Civil Construction</a>
                    <h3><a href="<?= base_url('philips') ?>" class="tittle">PT Philips</a></h3>
                </div>
            </div>
            <div class="project-item">
                <img src="<?= base_url('assets/') ?>img/project/civil-constructions/kapel-villa/villa-1-min.jpg" alt="projects">
                <div class="overlay"></div>
                <div class="projects-content">
                    <a href="#" class="category">Civil Construction</a>
                    <h3><a href="<?= base_url('kapel_villa') ?>" class="tittle">Kapel Villa</a></h3>
                </div>
            </div>
            <div class="project-item">
                <img src="<?= base_url('assets/') ?>img/project/civil-constructions/twa-wisata-alam/wisata-alam.jpg" alt="projects">
                <div class="overlay"></div>
                <div class="projects-content">
                    <a href="#" class="category">Civil Construction</a>
                    <h3><a href="<?= base_url('twa_wisata_alam') ?>" class="tittle">TWA Wisata Alam</a></h3>
                </div>
            </div>
        </div>
    </div>
</section>