<!-- <section class="page-header padding">
    <div class="container">
        <div class="page-content text-center">
            <h2>Our Clients</h2>
        </div>
    </div>
</section> -->
<section class="projects-section padding">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-lg-8 col-md-6 sm-padding">
                <div class="section-heading mb-40">
                    <span>Clients</span>
                    <h2>Client List</h2>
                </div>
            </div>
            <!-- <div class="col-lg-4 col-md-6 sm-padding text-right">
                <a href="#" class="default-btn">View All Projects</a>
            </div> -->
        </div>
        <div class="row">
            <div class="bd-example bd-example-tabs col-md-12">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Oil and Gas</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Marine and Ofshore</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Civil Construction</a>
                    </li>
                </ul>
                <div class="tab-content mt-4" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Client Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($oil_gas as $key => $value) { ?>
                                        <tr>
                                            <td class="text-center"><?= $key + 1 ?></td>
                                            <td>
                                                <strong class="text-black"><?= $value->perusahaan ?></strong>
                                                <p><?= $value->deskripsi ?></p>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Client Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($marine as $key => $value) { ?>
                                        <tr>
                                            <td class="text-center"><?= $key + 1 ?></td>
                                            <td>
                                                <strong class="text-black"><?= $value->perusahaan ?></strong>
                                                <p><?= $value->deskripsi ?></p>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Client Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($civil as $key => $value) { ?>
                                        <tr>
                                            <td class="text-center"><?= $key + 1 ?></td>
                                            <td>
                                                <strong class="text-black"><?= $value->perusahaan ?></strong>
                                                <p><?= $value->deskripsi ?></p>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>