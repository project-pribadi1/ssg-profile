<style>
    .mapouter {
        position: relative;
        text-align: right;
        height: 350px;
        width: 100%;
    }

    .gmap_canvas {
        overflow: hidden;
        background: none !important;
        height: 350px;
        width: 100%;
    }
</style>
<div class="mapouter">
    <!-- <section class="page-header padding">
        <div class="container">
            <div class="page-content text-center">
                <h2>Contact us</h2>
            </div>
        </div>
    </section> -->
    <section class="contact-section padding">
        <!-- <div class="dots"></div> -->
        <!-- <div class="container"> -->
        <div class="contact-wrap d-flex row pr-3">
            <div class="col-md-6">
                <div class="contact-info">
                    <h2>Contact Us</h2>
                    <h4>Address :</h4>
                    <p>Panbil Plaza Lantai 3, Jl. Ahmad Yani - Muka kuning Batam 29433, <br> Indonesia</p>
                    <!-- <h4> -->
                    <h4>Email :</h4>
                    <p>marudutsimalango@gmail.com</p>
                    <h4>Phone :</h4>
                    0778 371642
                    <h4>Fax:</h4>
                    <p>+62 778 371100</p>
                    <!-- </h4> -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="gmap_canvas">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.0799270952525!2d104.05583631475373!3d1.10232999919593!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31d988d4c07fea01%3A0x6d2f5fe28b1fd53b!2sPT.Sumber%20Sukses%20Ganda!5e0!3m2!1sid!2sid!4v1595757303399!5m2!1sid!2sid" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </section>