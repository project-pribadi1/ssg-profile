<div id="main-slider" class="dl-slider">
    <div class="single-slide">
        <div class="bg-img kenburns-top-right" style="background-image: url(<?= base_url('assets/') ?>img/home/banner-1.jpg);"></div>
        <div class="overlay"></div>
        <div class="slider-content-wrap d-flex align-items-center text-left">
            <div class="container">
                <div class="slider-content" id="auto-hide">
                    <div class="dl-caption">
                        <div class="inner-layer">
                            <h1 data-animation="fade-in-right" data-delay="1s">PT Sumber Sukses Ganda</h1>
                        </div>
                    </div>
                    <div class="dl-caption">
                        <div class="inner-layer">
                            <h3 data-animation="fade-in-left" data-delay="2s">We Provide Construction Services.</h3>
                        </div>
                    </div>
                    <div class="dl-caption small">
                        <div class="inner-layer">
                            <h5 data-animation="fade-in-left" data-delay="3s">We provide steel construction for oil and Gas industries, marine <br> & offshore industries, refinery & powerplant industries, workshop and factory building.</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="single-slide">
        <div class="bg-img kenburns-top-right" style="background-image: url(<?= base_url('assets/') ?>img/home/banner-2.jpg);"></div>
        <div class="overlay"></div>
        <div class="slider-content-wrap d-flex align-items-center text-center">
            <div class="container">
                <div class="slider-content" id="auto-hide2">
                    <div class="dl-caption">
                        <div class="inner-layer">
                            <h1 data-animation="fade-in-top" data-delay="1s">PT SUMBER SUKSES GANDA</h1>
                        </div>
                    </div>
                    <div class="dl-caption">
                        <div class="inner-layer">
                            <h3 data-animation="fade-in-bottom" data-delay="2s">We are professional <br>for building construction.</h3>
                        </div>
                    </div>
                    <div class="dl-caption">
                        <div class="inner-layer">
                            <h5 data-animation="fade-in-bottom" data-delay="3s">We have provided complete remodeling and construction solutions for <br>residential and commercial properties in cities.</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="single-slide">
        <div class="bg-img kenburns-top-right" style="background-image: url(<?= base_url('assets/') ?>img/home/banner-3.jpg);"></div>
        <div class="overlay"></div>
        <div class="slider-content-wrap d-flex align-items-center text-right">
            <div class="container">
                <div class="slider-content" id="auto-hide3">
                    <div class="dl-caption">
                        <div class="inner-layer">
                            <h1 data-animation="fade-in-left" data-delay="1s">PT SUMBER SUKSES GANDA</h1>
                        </div>
                    </div>
                    <div class="dl-caption">
                        <div class="inner-layer">
                            <h3 data-animation="fade-in-right" data-delay="2s">We will be happy to take care <br>of your construction works.</h3>
                        </div>
                    </div>
                    <div class="dl-caption">
                        <div class="inner-layer">
                            <h5 data-animation="fade-in-right" data-delay="3s">We have provided complete remodeling and construction solutions for <br>residential and commercial properties in cities.</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="promo-section padding" id="about" data-aos="fade-up" data-aos-anchor-placement="top-center">
    <div class="container">
        <div class="col-12">
            <center>
                <h5>About</h5>
                <h2>PT SUMBER SUKSES GANDA</h2>
                <h4>Since 2005, We are <strong>Panbil Group of Company</strong></h4>
                <h4 style="color: #8d9aa8;">Marine - Offshore - Oil & Gas - Petrochemicals - Industries Contractor & Supplier.</h4>
            </center>
        </div>
        <div class="row promo-wrap mt-5">
            <div class="col-lg-4 col-sm-6">
                <div class="promo-item box-shadow text-center wow fadeInUp" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="3000">
                    <h3>Vision</h3>
                    <p>
                        Opening a job opportunity and skill development. Creating improvisation to add value for customer’s solution. To do with safe, control and green environment.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="promo-item box-shadow text-center wow fadeInUp" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="3000">
                    <h3>Mission</h3>
                    <p>
                        Building professionalism, integrity, honesty and fairness to handle the job by placing the right person and the right job. To do with realistic cost, efficient and best quality for all jobs. To deliver product with high quality and best services.
                    </p>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6">
                <div class="promo-item box-shadow text-center wow fadeInUp" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="3000">
                    <h3>Commitment to Clients</h3>
                    <p>Meet Target and best project schedule - Meet Quality of Product and services.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="service-section section-2 bg-grey padding" data-aos="fade-up" data-aos-anchor-placement="top-center">
    <div class="dots"></div>
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-lg-12 sm-padding">
                <div class="row services-list">
                    <div class="col-md-4 padding-15">
                        <div class="service-item box-shadow wow fadeInUp" data-aos="zoom-in" data-aos-delay="100" data-aos-duration="3000">
                            <i class="fas fa-wave-square"></i>
                            <h4>Piping & Plumbing Work</h4>
                            <p>Plumbing System, Pipe Spool Facrication, Pipe Installation and Hydrotest </p>
                        </div>
                    </div>
                    <div class="col-md-4 padding-15 offset-top">
                        <div class="service-item box-shadow wow fadeInUp" data-aos="zoom-in" data-aos-delay="200" data-aos-duration="3000">
                            <i class="fas fa-paint-roller"></i>
                            <h4>Blasting and Painting Work</h4>
                            <p>Sand Blasting and Power Tooling, Painting and Touh up</p>
                        </div>
                    </div>

                    <div class="col-md-4 padding-15">
                        <div class="service-item box-shadow wow fadeInUp" data-aos="zoom-in" data-aos-delay="300" data-aos-duration="3000">
                            <i class="fas fa-charging-station"></i>
                            <h4>Electrical & Instrument Work</h4>
                            <p>MSB Panel Builder, Electrical Steel Work Fabrication & Installation, Pulling Cable & Tie Up, Line Check and Measuring & Tag, and Termination & Connection, Partial Test, General Test, Sea Trial .</p>
                        </div>
                    </div>

                    <div class="col-md-4 padding-15 ">
                        <div class="service-item box-shadow wow fadeInUp" data-aos="zoom-in" data-aos-delay="400" data-aos-duration="3000">
                            <i class="fas fa-ship"></i>
                            <h4>Steel Construction Engineering & Design</h4>
                            <p>
                                Steel Construction, Ship Building, Hull Fabrication and Erection, Out Fittings Fabrication and Installation, and Firing and Bending
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 padding-10 offset-top">
                        <div class="service-item box-shadow wow fadeInUp" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="3000">
                            <i class="fas fa-city"></i>
                            <h4>Civil Construction Engineering & Design</h4>
                            <p>
                                Civil Building Construction
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-12 sm-padding mt-5" data-aos="fade-up" data-aos-anchor-placement="top-center">
                <div class="service-content wow fadeInLeft">
                    <!-- <h4>Our Services</h4> <br> -->
                    <h3>Commitment to Health & Safety</h3>
                    <h5>Contracting is committed to health and safety of its workforce and the preservation of the environment for benefit of families today and future generations :</h5>

                    <li>
                        An established global safety, health and environment management system based upon regulatory requirements and recognized best practices.
                    </li>
                    <li>
                        We will continually promote employee safety on and off the job.
                    </li>
                    <li>
                        We believe all occupational injuries and illnesses are preventable.
                    </li>
                    <li>
                        Regularly assessing and auditing all our operations to measure conformance to our policies and standards with the goal of continuous improvement.
                    </li>
                    <li>
                        Working with customers, suppliers, local communities, government agencies, and industry groups
                        to meet our safety, health and environmental responsibilities.
                    </li>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end main -->

<script>
    $(document).ready(function() {
        // function hidee_elemetn() {
        // setInterval(() => {
        //     $('#auto-hide').hide();
        //     $('#auto-hide2').hide();
        //     $('#auto-hide3').hide();
        // }, 10000);
        setTimeout(() => {
            $('.slider-content').hide();
            // $('#auto-hide').hide();
        }, 20000);
        // setTimeout(() => {
        //     $('#auto-hide2').hide();
        // }, 40000);
        // setTimeout(() => {
        //     $('#auto-hide3').hide();
        // }, 40000);
        // }
    })
</script>