<section class="promo-section padding" id="about" data-aos="fade-up" data-aos-anchor-placement="top-center">
    <div class="container">
        <div class="row promo-wrap mt-5">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <h3>Form input</h3>
                <form action="<?= base_url('panel/insert') ?>" method="post">
                    <div class="form-group">
                        <label for="">Jenis</label>
                        <select name="jenis" id="jenis" class="form-control">
                            <option value="1">Client</option>
                            <option value="2">Project</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="1">Oil and Gas</option>
                            <option value="2">Marine Offshore</option>
                            <option value="3">Civil Construction</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Perusahaan</label>
                        <textarea name="perusahaan" id="perusahaan" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Deskripsi</label>
                        <textarea name="deskripsi" id="deskripsi" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <button onclick="" type="button" class="btn btn-danger">Back to home</button>
                        <button type="submit" class="btn btn-primary float-right">Save</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
</section>