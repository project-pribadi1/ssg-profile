<section class="promo-section padding" id="about" data-aos="fade-up" data-aos-anchor-placement="top-center">
    <div class="container">
        <div class="row">
            <div class="bd-example bd-example-tabs col-md-12">
                <?= $this->session->flashdata('message'); ?>
                <a class="btn btn-sm btn-info float-right" href="#" data-toggle="modal" data-target="#modal-form"><i class="fas fa-fw fa-plus"></i> Add</a>
                <h3 class="mb-4">List Master</h3>
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="pills-projecs-tab" data-toggle="pill" href="#pills-projecs" role="tab" aria-controls="pills-projecs" aria-selected="true">Projects</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="pills-clients-tab" data-toggle="pill" href="#pills-clients" role="tab" aria-controls="pills-clients" aria-selected="false">Clients</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="pills-projecs" role="tabpanel" aria-labelledby="pills-projecs-tab">
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Perusahaan</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($projects as $key => $value) { ?>
                                        <tr>
                                            <td><?= $key + 1 ?></td>
                                            <td><?= $value->perusahaan ?></td>
                                            <td><?= $value->deskripsi ?></td>
                                            <td>
                                                <?php
                                                if ($value->status == 1) {
                                                    echo 'Oil and Gas';
                                                } elseif ($value->status = 2) {
                                                    echo 'Marine and Offshore';
                                                } else {
                                                    echo 'Civil Construction';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="#" onclick="edit_summary(<?= $value->id ?>)" data-toggle="modal" data-target="#modal-form"><i class="fas fa-fw fa-pencil-alt"></i></a>
                                                <a href="#" onclick="confirm_delete(<?= $value->id ?>)"><i class="fas fa-fw fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-clients" role="tabpanel" aria-labelledby="pills-clients-tab">
                        <div class="table-responsive col-md-12">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Perusahaan</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($clients as $key => $value) { ?>
                                        <tr>
                                            <td><?= $key + 1 ?></td>
                                            <td><?= $value->perusahaan ?></td>
                                            <td><?= $value->deskripsi ?></td>
                                            <td>
                                                <?php
                                                if ($value->status == 1) {
                                                    echo 'Oil and Gas';
                                                } elseif ($value->status = 2) {
                                                    echo 'Marine and Offshore';
                                                } else {
                                                    echo 'Civil Construction';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="#" onclick="edit_summary(<?= $value->id ?>)" data-toggle="modal" data-target="#modal-form"><i class="fas fa-fw fa-pencil-alt"></i></a>
                                                <a href="#" onclick="confirm_delete(<?= $value->id ?>)"><i class="fas fa-fw fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-form" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('panel/insert') ?>" method="post">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Jenis</label>
                        <select name="jenis" id="jenis" class="form-control">
                            <option value="1">Client</option>
                            <option value="2">Project</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="1">Oil and Gas</option>
                            <option value="2">Marine Offshore</option>
                            <option value="3">Civil Construction</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Perusahaan</label>
                        <textarea name="perusahaan" id="perusahaan" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Deskripsi</label>
                        <textarea name="deskripsi" id="deskripsi" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function edit_summary(id) {
        $.ajax({
            url: "<?= base_url() . 'panel/edit'; ?>",
            async: false,
            type: 'POST',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(data) {
                $('#id').val(data.id);
                $('#perusahaan').val(data.perusahaan);
                $('#deskripsi').val(data.deskripsi);
                $('#jenis').val(data.jenis);
                $('#status').val(data.jenis);
            }
        });
    }

    function confirm_delete(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.href = `<?= base_url('panel/delete/') ?>${id}`;
            }
        })
    }
</script>